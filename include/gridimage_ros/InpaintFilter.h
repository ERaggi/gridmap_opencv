#pragma once

#include <filters/filter_base.h>
#include <grid_map_cv/grid_map_cv.hpp>

#include <vector>
#include <string>

namespace grid_map {

// Uses OpenCV function to inpaint/fill holes in the input layer

template<typename T>
class InpaintFilter : public filters::FilterBase<T>
{
public:
    InpaintFilter();


    virtual ~InpaintFilter();

    // configure the filter from parameters on the Parameter Server

    virtual bool configure();

    // Add a new output layer to the map
    // Uses the OpenCV function inpaint holes in the layer
    // Saves to filled map in the output layer
    // @param mapIn grid map containing input layer
    // @param mapOut grid map containing mapIn and inpainted input layer

    virtual bool update(const T& mapIn, T& mapOut);

private:
    double radius;

    std::string inputLayer;

    std::string outputLayer;

};

}
