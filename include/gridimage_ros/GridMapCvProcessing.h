#pragma once
#include <grid_map_core/grid_map_core.hpp>
#include <cv_bridge/cv_bridge.h>
#include "Eigen/Eigen"

namespace grid_map {


// Processing of grid map with
// OpenCV methods


class GridMapCvProcessing
{
public:
    GridMapCvProcessing();

    // Change the resolution of a grid map with the help of openCV
    // interpolation algorithm.
    // @param[in] gridMapSource the source of the grid map
    // @param[out] gridMapResult the resulting grid map with the desired resolution
    // @param[in] resolution the desired resolution
    // @param[in] (optional) interpolationAlgorithm the interpolation method
    // @return true if success, flase otherwise

    static bool changeResolution(const GridMap& gridMapSource,
                                 GridMap& gridMapResult,
                                 const double resolution,
                                 const int interpolationAlgorithm = cv::INTER_CUBIC);
};

} // namespace grid_map END
